INF319 - Projeto e Implementação Orientados a Objetos

Aluno: Guilherme Kayo Shida

Comentário:

- Existe uma relacionamento associativo utilizando a classe ContratoDeTrabalho entre as classes Pessoa e Companhia;
- A classe ContratoDeTrabalho possui uma coleção de Contratos que posuem referências ao empregado e ao empregador;
- A classe Pessoa sabe quais contratos vigentes ele possui através do atributo contratos;
- A classe Companhia sabe quais contratos vigentes ele possui através do atributo contratos;
- Este tipo de associação aumenta a complexidade do código, talvez para este tipo de problema uma solução mais simples seria uma melhor solução.