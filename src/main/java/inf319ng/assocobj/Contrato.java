package inf319ng.assocobj;

public class Contrato {
	private double salario;
	private Pessoa empregado;
	private Companhia empregador;
	
	public Contrato(Companhia companhia, Pessoa pessoa, double salario) {
		this.empregador = companhia;
		this.empregado = pessoa;
		this.salario = salario;
	}
	
	public double getSalario() {
		return salario;
	}
	public Pessoa getEmpregado() {
		return empregado;
	}
	public Companhia getEmpregador() {
		return empregador;
	}
}
