package inf319ng.assocobj;

import java.util.Iterator;
import java.util.Set;

public class Companhia {
	private String nome_companhia;
	private ContratosDeTrabalho contratos;

	public Companhia(String nome_companhia) {
		this.nome_companhia = nome_companhia;
		this.contratos = null;
	}

	public String getNome() {
		return nome_companhia;
	}

	public ContratosDeTrabalho getContratos() {
		return contratos;
	}

	public void setContratos(ContratosDeTrabalho contratos) {
		this.contratos = contratos;
	}

	public void emprega(Pessoa pessoa, double salario) {
		this.contratos.emprega(this, pessoa, salario);
	}

	public void demite(Pessoa pessoa) {
		this.contratos.demite(this, pessoa);
	}

	public double custoTotal() {
		double custoTotal = 0.0;
		Set<Contrato> empregadosContrato = contratos.getEmpregados(this);
		
		for (Contrato contrato : empregadosContrato) {
			custoTotal += contrato.getSalario();
		}
		
		return custoTotal;
	}
}
