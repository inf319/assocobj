package inf319ng.assocobj;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ContratosDeTrabalho {
	private Map<Pessoa, HashSet<Contrato>> contratosEmpregados;
	private Map<Companhia, HashSet<Contrato>> contratosEmpregadores;
	
	public ContratosDeTrabalho() {
		contratosEmpregados = new HashMap<Pessoa, HashSet<Contrato>>();
		contratosEmpregadores = new HashMap<Companhia, HashSet<Contrato>>();
	}
	
	public Set<Contrato> getEmpregadores(Pessoa pessoa) {
		return contratosEmpregados.get(pessoa);
	}
	
	public Set<Contrato> getEmpregados(Companhia companhia) {
		return contratosEmpregadores.get(companhia);
	}
	
	public void emprega(Companhia companhia, Pessoa pessoa, double salario) {
		Contrato novoContrato = new Contrato(companhia, pessoa, salario);
		
		HashSet<Contrato> contratosDaPessoa = contratosEmpregados.get(pessoa);
		if (contratosDaPessoa == null) {
			contratosDaPessoa = new HashSet<Contrato>();
		}
		contratosDaPessoa.add(novoContrato);
		
		HashSet<Contrato> contratosDaEmpresa = contratosEmpregadores.get(companhia);
		if (contratosDaEmpresa == null) {
			contratosDaEmpresa = new HashSet<Contrato>();
		}
		contratosDaEmpresa.add(novoContrato);
		
		contratosEmpregados.put(pessoa, contratosDaPessoa);
		contratosEmpregadores.put(companhia, contratosDaEmpresa);
	}

	public void demite(Companhia companhia, Pessoa pessoa) {
		HashSet<Contrato> contratosDaPessoa = contratosEmpregados.get(pessoa);
		HashSet<Contrato> contratosDaEmpresa = contratosEmpregadores.get(companhia);
		
		for (Contrato contrato : contratosDaPessoa) {
			if (contrato.getEmpregador() == companhia) {
				contratosDaPessoa.remove(contrato);
				break;
			}
		}
		for (Contrato contrato : contratosDaEmpresa) {
			if (contrato.getEmpregado() == pessoa) {
				contratosDaEmpresa.remove(contrato);
				break;
			}
		}
	}
}
