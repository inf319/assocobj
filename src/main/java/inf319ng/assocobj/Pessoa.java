package inf319ng.assocobj;

import java.util.HashSet;
import java.util.Set;

public class Pessoa {
	private String nome;
	private String sobrenome;
	private ContratosDeTrabalho contratos;

	public Pessoa(String nome, String sobrenome) {
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.contratos = null;
	}

	public ContratosDeTrabalho getContratos() {
		return contratos;
	}

	public void setContratos(ContratosDeTrabalho contratos) {
		this.contratos = contratos;
	}

	public String getNome() {
		return nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}
	
	public Set<Companhia> getEmpregos() {
		Set<Contrato> meusContratos = contratos.getEmpregadores(this);
		Set<Companhia> empregos = new HashSet<Companhia>();
		
		for (Contrato contrato : meusContratos) {
			empregos.add(contrato.getEmpregador());
		}
		
		return empregos;
	}
	
	public double getSalarioTotal() {
		double salarioTotal = 0.0;
		Set<Contrato> meusContratos = contratos.getEmpregadores(this);
		
		for (Contrato contrato : meusContratos) {
			salarioTotal += contrato.getSalario();
		}
		
		return salarioTotal;
	}
}
